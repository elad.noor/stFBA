\babel@toc {english}{}
\beamer@sectionintoc {1}{Importance of Thermodynamics}{2}{0}{1}
\beamer@sectionintoc {2}{Short introduction to extreme pathways}{6}{0}{2}
\beamer@sectionintoc {3}{Thermodynamics and Energy Generating Cycles}{9}{0}{3}
\beamer@sectionintoc {4}{Gibbs Energy Dissipation}{34}{0}{4}
