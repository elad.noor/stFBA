\pdfminorversion=4
\documentclass[12pt]{beamer}
\usetheme{Madrid}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}

\usepackage{libertine}
\usepackage[version-1-compatibility]{siunitx}
\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
\usepackage{xcolor}
\usepackage{cancel}
\usepackage{ulem}
\usepackage{upgreek}
\usepackage{amsmath}

\usefonttheme[onlymath]{serif}
\DeclareMathOperator{\Ima}{Im}

\definecolor{expath1}{RGB}{15,178,161}
\definecolor{expath2}{RGB}{25,25,155}
\definecolor{expath3}{RGB}{128,0,0}

\author{Elad Noor}
\institute[ETH]{
	Institute for Molecular Systems Biology\\
	\medskip
	\includegraphics[height=0.5in]{figures/imsb_logo}~~~\includegraphics[height=0.5in]{figures/eth_logo}\\[3mm]
	\textit{noor@imsb.biol.ethz.ch}
}
\title[semi-thermodynamic FBA]{Removing both Internal and Unrealistic Energy-Generating Cycles in Flux Balance Analysis}

%subtitle{Enzyme Cost Minimization}

%logo{}

%institute{}

%date{}

%subject{}

%setbeamercovered{transparent}

%setbeamertemplate{navigation symbols}{}

\begin{document}
\maketitle

\section{Importance of Thermodynamics}

\begin{frame}[t]
\frametitle{The laws of Thermodynamics}
	\begin{itemize}
		\item First law: conservation of energy
		\item<2-> $\mathbf{\Delta_r G'} \perp \ker(\mathbf{S_{int}}) ~\leftrightarrow~ \exists \mathbf{g} \in \mathbb{R}^m ~~\text{s.t.}~~ \mathbf{\Delta_r G'} = \mathbf{S_{int}}^\top \mathbf{g}$
	\end{itemize}
	\vspace{1cm}
	\begin{itemize}
		\item Second law: entropy of the universe can only increase
		\item<3-> $\forall v_i > 0~~~\Delta_r G'_i < 0$
	\end{itemize}
\end{frame}

\begin{frame}[t]
	\frametitle{Should we integrate thermodynamics in metabolic models?}
	\begin{itemize}
		\item Feasible \textbf{metabolite concentration ranges} are minimally affected by thermodynamic constraints
		\cite{henry_thermodynamics-based_2007}
		\item For \textbf{identifying EFMs}: in the best case can potentially eliminate 50\% of them \cite{peres_how_2017}		
		\item Supplying a \textbf{complete and precise list of} $\Delta_r G'^\circ$ values for a model is \textbf{difficult}
		\item We know very little about \textbf{internal concentrations}
	\end{itemize}
\end{frame}

\section{Short introduction to extreme pathways}

\begin{frame}[t]
	\frametitle{Flux Balance}
	
	\begin{equation}
	\frac{dx}{dt} = \mathbf{S} \cdot \mathbf{v}
	\end{equation}
	The steady-state assumption imposes a constraint that all concentrations are constant over time, therefore
	\begin{equation}
	\mathbf{S} \cdot \mathbf{v} = 0 
	\end{equation}
	Most constraint-based applications come with additional constraints on individual fluxes, i.e.
	\begin{equation}
		\mathbf{v}^L \leq \mathbf{v} \leq \mathbf{v}^U
	\end{equation}
\end{frame}
	
\begin{frame}[t]
	\frametitle{Extreme Pathways}
	\begin{tabular}{cl|cc}
		&& \multicolumn{2}{c}{exchange fluxes} \\
		Type & Name & primary & currency \\
		\hline
		\color{expath1}{I} & \color{expath1}{Primary systemic pathway} & $\ge 1$ & $\ge 0$ \\
		\color{expath2}{II} & \color{expath2}{Futile cycle} & $0$ & $\ge 1$ \\
		\color{expath3}{III} & \color{expath3}{Internal cycle} & $0$ & $0$ \\
		\hline
	\end{tabular}
	\vspace{0.5cm}\\
	\begin{center}
	\includegraphics[width=0.7\textwidth]{figures/figure1a.pdf}
	\end{center}
\end{frame}

\begin{frame}[t]
\frametitle{Energy Generating Cycles (EGCs)}
\begin{tabular}{cl|cc}
	&& \multicolumn{2}{c}{exchange fluxes} \\
	Type & Name & primary & currency \\
	\hline
	\color{expath1}{I} & \color{expath1}{Primary systemic pathway} & $\ge 1$ & $\ge 0$ \\
	\color{expath2}{II} & \color{expath2}{Futile/energy-generating cycle} & $0$ & $\ge 1$ \\
	\color{expath3}{III} & \color{expath3}{Internal cycle} & $0$ & $0$ \\
	\hline
\end{tabular}
\vspace{0.5cm}\\
\begin{center}
	\includegraphics[width=0.7\textwidth]{figures/figure1b.pdf}
\end{center}
\end{frame}

\section{Thermodynamics and Energy Generating Cycles}

\begin{frame}[t]
	\frametitle{Problems caused by Type II and Type III Pathways}
	\begin{itemize}
		\item Internal cycles don't affect the maximal biomass rate
		\item But, they cause Flux Variability Analysis (FVA) to overestimate flux ranges
		\item<2-> Type II futile cycles are physiologically relevant and are usually avoided by regulation mechanisms
		\item<3-> Type II EGCs, however, can artificially increase the maximal biomass rate
	\end{itemize}
\end{frame}

\begin{frame}[t]
	\frametitle{Energy Generating Cycle in \textit{E. coli} model}
	\begin{center}
	\includegraphics[width=0.6\textwidth]{figures/figure2.pdf}
	\end{center}
	\begin{columns}
		\begin{column}{0.6\textwidth}<2->
		\begin{tiny}
			\begin{tabular}{l|l}
				\label{table:egc_example}
				Reaction & Formula\\\hline
				MDH & mal\_\_L-c + nad\_c = h\_c + nadh\_c + oaa\_c\\
				MOX	& h2o2\_c + oaa\_c = mal\_\_L\_c + o2\_c\\
				Htex & h\_e = h\_p\\
				EX\_h\_e & = h\_e\\
				SPODM & 2.0 h\_c + 2.0 o2s\_c = h2o2\_c + o2\_c\\
				NADH17pp & 4.0 h\_c + mqn8\_c + nadh\_c = mql8\_c + nad\_c + 3.0 h\_p\\
				QMO3 & mql8\_c + 2.0 o2\_c = 2.0 h\_c + mqn8\_c + 2.0 o2s\_c\\
				ATPS4rpp & adp\_c + pi\_c + 4.0 h\_p = atp\_c + 3.0 h\_c + h2o\_c\\\hline
				Total & adp\_c + pi\_c = atp\_c + h2o\_c
			\end{tabular}
		\end{tiny}
		\end{column}
		\begin{column}{0.4\textwidth}<3->
			H$_2$O$_2$ producing and consuming reactions (SPODM and MOX) are knocked out by default in iJO1366
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[t]
	\frametitle{Most automatically reconstructed metabolic models contain EGCs}
	\begin{columns}
		\begin{column}{0.5\textwidth}
			Figure from:\\
			\textit{Erroneous energy-generating cycles in published genome scale metabolic networks: Identification and removal} \footnote[frame]{\cite{fritzemeier_erroneous_2017}}
		\end{column}
		\begin{column}{0.5\textwidth}
			\includegraphics[width=\textwidth]{figures/journal_pcbi_1005494_g003.png}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[t]
	\frametitle{Localized Thermodynamic Bottlenecks}
	\begin{center}
		MOX~:~~~h2o2\_c + oaa\_c = mal\_\_L\_c + o2\_c
		\vspace{0.5cm}\\
		\includegraphics[scale=0.25]{figures/equilibrator_MOX.png}
		\begin{equation}\Delta_r G' = \sum_i \nu_i~\Delta_f G' = \sum_i \nu_i~(\Delta_f G'^\circ + R~T~\ln{c_i}) < 0
		\end{equation}
	\end{center}
\end{frame}

\begin{frame}[t]
\frametitle{Localized Thermodynamic Bottlenecks}
	\begin{columns}[b]
	\begin{column}{0.45\textwidth}
		\begin{footnotesize}
			By defining possible concentration ranges $\forall i~b^{L}_i \leq c_i \leq b^{U}_i$,	we can calculate the lower bound

			\begin{align*}
				\Delta_r G'_{min} &= \sum_i \nu_i~\Delta_f G'^\circ \\
					& + \underbrace{RT\sum_{\nu_i<0} \nu_i \ln{b^{U}_i}}_\text{substrates} + \underbrace{RT\sum_{\nu_i>0} \nu_i \ln{b^{L}_i}}_\text{products}
			\end{align*}					
		$\Delta_r G'_{min} < 0 ~\rightarrow~$ forward feasible\\
		\only<2->{
		$\Delta_r G'_{max} > 0 ~\rightarrow~$ backward feasible \footnote[frame]{$\Delta_r G'_{max}$ is similarly defined with substrates and products reversed}}
		\end{footnotesize}
		\end{column}
		\begin{column}{0.45\textwidth}<3->
			\includegraphics[width=\textwidth]{figures/localized_bottleneck.pdf}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\frametitle{Distributed Thermodynamic Bottlenecks}
	\begin{columns}
		\begin{column}{0.5\textwidth}
	A pathway comprising feasible reactions can still be infeasible. 
The reason is, that some metabolites appear both as substrates and as products in different reactions:
		\[\mathbf{\Delta_r G'} = \mathbf{S}^\top \left( \mathbf{\Delta_f G'^\circ} + RT \cdot \ln \mathbf{c}\right) < \mathbf{0} \]
		\end{column}
		\begin{column}{0.5\textwidth}
			\only<1>{
			\includegraphics[width=0.9\textwidth]{figures/distributed_bottleneck.pdf}
			}
			\only<2>{
			\includegraphics[width=0.9\textwidth]{figures/distributed_bottleneck2.pdf}
			}
		\end{column}
	\end{columns}

\end{frame}

\begin{frame}[t]
\frametitle{Energy Generating Cycles}
	An EGC containing a distributed bottleneck is the especially problematic.
	For example, ADK1, PYK and PPS are all theoretically reversible. If used together, they would form an EGC:
	\begin{center}
		\includegraphics[width=0.6\textwidth]{figures/figure3.pdf}
	\end{center}

	However, we cannot exclude this EGC by restricting a single reaction's direction.
\end{frame}

\begin{frame}[t]
	\frametitle{Thermodynamic Flux Balance Analysis}
	TFBA\footnote{\cite{henry_thermodynamics-based_2007}} \hspace{3cm} $\mathbf{v^*} = \mathrm{arg\max_v} {~\mathbf{c}^\top\mathbf{v}}$\\
	such that:
	\begin{small}
		\begin{align*}
		\mathbf{S} \cdot \mathbf{v} &= \mathbf{0} \\
		\mathbf{v}^L &\leq \mathbf{v} \leq \mathbf{v}^U\\
		\only<2->{\mathbf{\Delta_f G'^\circ} + RT\cdot\ln(\mathbf{b}^L) &\leq \mathbf{g} \leq \mathbf{\Delta_f G'^\circ}  + RT\cdot\ln(\mathbf{b}^U)\\}
		\only<3->{\mathbf{\Delta_r G'} &= \mathbf{S}_\text{ int}^\top ~ \mathbf{g}\\
		\text{sign}(\mathbf{\Delta_r G'}) &= -\text{sign}(\mathbf{v})\\}
		\end{align*}		
	\end{small}
	
\end{frame}

\begin{frame}[t]
\frametitle{Loopless Flux Balance Analysis}
	ll-FBA\footnote{\cite{schellenberger_elimination_2011}} \hspace{3cm} $\mathbf{v^*} = \mathrm{arg\max_v} {~\mathbf{c}^\top\mathbf{v}}$\\
	such that:
	\begin{small}
		\begin{align*}
		\mathbf{S} \cdot \mathbf{v} &= \mathbf{0} \\
		\mathbf{v}^L &\leq \mathbf{v} \leq \mathbf{v}^U\\
		\text{\sout{$\mathbf{\Delta_f G'^\circ} + RT\cdot\ln(\mathbf{b}^L)$}} &\text{\sout{$\leq \mathbf{g} \leq \mathbf{\Delta_f G'^\circ} + RT\cdot\ln(\mathbf{b}^U)$}}\\
		\only<1>{\mathbf{\Delta_r G'} &= \mathbf{S}_\text{ int}^\top ~ \mathbf{g}\\}
		\only<2->{\mathbf{\Delta_r G'} &\perp \ker{(\mathbf{S}_\text{ int})}\\}
		\text{sign}(\mathbf{\Delta_r G'}) &= -\text{sign}(\mathbf{v})\\
		\end{align*}
	\end{small}
	\begin{itemize}
		\item<3> No $\mathbf{\Delta_f G'^\circ}$ information required
		\item<3> Removes all internal (Type III) cycles~\footnote{\cite{noor_proof_2012}}
		\item<3> Does \textbf{not} remove any other solutions (such as EGCs)
	\end{itemize}
\end{frame}

\begin{frame}[t]
	\frametitle{ll-FBA is sound and complete}
	\centering
	\includegraphics[width=0.8\textwidth]{figures/gordan.pdf}
	\cite{noor_proof_2012}
\end{frame}

\begin{frame}
\frametitle{Loopless Flux Balance Analysis}
However, ll-FBA will not remove an EGC:
\begin{center}
	\includegraphics[width=0.6\textwidth]{figures/gordan_EGC.pdf}
\end{center}

We know this cycle is infeasible, because $g_{ADP} \ll g_{ATP}$, but without TFBA
constraints, this can be violated.
\end{frame}

\begin{frame}[t]
\frametitle{Semi-Thermodynamic Flux Balance Analysis}
st-FBA \footnote{\cite{noor_removing_2018}}
\hspace{3cm} $\mathbf{v^*} = \mathrm{arg\max_v} {~\mathbf{c}^\top\mathbf{v}}$\\
such that:
\begin{small}
	\begin{align*}
		\mathbf{S} \cdot \mathbf{v} &= \mathbf{0} \\
		\mathbf{v}^L &\leq \mathbf{v} \leq \mathbf{v}^U\\
		\forall i \in \mathcal{C}~~~~\Delta_f G'^\circ_i + RT\cdot\ln(b^L_i) &\leq g_i \leq \Delta_f G'^\circ_i + RT\cdot\ln(b^U_i)\\
		\mathbf{\Delta_r G'} &= \mathbf{S}_\text{ int}^\top ~ \mathbf{g}\\
		\text{sign}(\mathbf{\Delta_r G'}) &= -\text{sign}(\mathbf{v})\\
	\end{align*}		
\end{small}
where $\mathcal{C}$ is the set of all currency and external metabolites.
\end{frame}

\begin{frame}[t]
\frametitle{Semi-Thermodynamic Flux Balance Analysis}
	\begin{tabular}{l|r|r|r}
		\label{table:potentials}
		metabolite & $b^L$ & $b^h$ & $\Delta_f G'^\circ$ [kJ/mol] \\ \hline
		glucose & $1$ $\upmu$M & $100$ mM & $-430$ \\
		acetate & $1$ $\upmu$M & $100$ mM & $-247$ \\
		CO$_2$ & $5$ $\upmu$M & $100$ mM & $-386$ \\
		O$_2$ & $1$ $\upmu$M & $100$ mM & $16$ \\
		NH$_3$ & $1$ $\upmu$M & $100$ mM & $79$ \\
		ATP & $9.63$ mM & $9.63$ mM & $-2296$ \\
		ADP & $0.56$ mM & $0.56$ mM & $-1424$ \\
		AMP & $0.28$ mM & $0.28$ mM & $-549$ \\
		orthophosphate & $1$ mM & $10$ mM & $-1056$ \\
		pyrophosphate & $1$ $\upmu$M & $1$ mM & $-1939$ \\
		H$^+$ (cytoplasm) & $10^{-7.6}$ M & $10^{-7.6}$ M & $0$ \\
		H$^+$ (extracellular) & $10^{-7.0}$ M & $10^{-7.0}$ M & $0$ \\
		H$_2$O & 1 & 1 & $-157.6$
	\end{tabular}
\end{frame}

\begin{frame}
\frametitle{Semi-Thermodynamic Flux Balance Analysis}
The new st-FBA constraints are sufficient to eliminate EGCs:
\begin{center}
	\includegraphics[width=0.6\textwidth]{figures/gordan_EGC2.pdf}
\end{center}
The constraints on $g_{ADP}$ and $g_{ATP}$ do not allow the cycle to operate in the energy generating direction.
\end{frame}

\section{Gibbs Energy Dissipation}

\begin{frame}[t]
\frametitle{Gibbs Energy Dissipation Rates}

For some applications, it is interesting to calculate the dissipation rate of Gibbs energy\footnote{Niebel, Leupold \& Heinemann, Nature Metabolism (in press)}. For every internal reaction $i$
\begin{equation}
	g^{diss}_i ~=~ -\Delta_r G'_i ~\cdot~ v_i
\end{equation}
\only<2->{
The total dissipation rate of the cell is
\begin{align*}
g^{diss} &= -\mathbf{\Delta_r G'_j}^\top \mathbf{v_{int}} = -\mathbf{\Delta_f G'}^\top~\mathbf{S_{int}}~\mathbf{v_{int}} \\
&= \mathbf{\Delta_f G'}^\top~\mathbf{S_{ext}}~\mathbf{v_{ext}}
\end{align*}
or in other words, it is enough to calculate the difference in the $\Delta_f G'$ of the external inputs and outputs. The parameters used in stFBA are sufficient for this.
}
\end{frame}


\begin{frame}[t]
\frametitle{Gibbs Energy Dissipation Rates}

Example, 41 flux measurements using wild-type and mutated \textit{E. coli} in different conditions

\includegraphics[width=0.7\textwidth]{figures/gdiss_simple.pdf}


\end{frame}


%------------------------------------------------

\begin{frame}[t]
	\frametitle{References}
	\bibliographystyle{apalike}
	\begin{tiny}
	    \bibliography{thermo.bib}
	\end{tiny}

\end{frame}

%------------------------------------------------

\begin{frame}[t]
	\frametitle{Acknowledgments}
	\begin{itemize}
		\item People:
		\begin{itemize}
			\item[-] Uwe Sauer - ETH, Z\"{u}rich
			\item[-] Wolfram Liebermeister -- INRA, Jouy en Joses
			\item[-] Stefan Schuster -- FSU, Jena
			\item[-] Matthias Heinemann -- U. Groningen
		\end{itemize}
		\item Slides, Code, and Manuscript:
		\begin{itemize}
			\item[-] \href{https://gitlab.com/elad.noor/stFBA}{https://gitlab.com/elad.noor/stFBA}
			\item[-] \href{https://arxiv.org/abs/1803.04999}{https://arxiv.org/abs/1803.04999}
		\end{itemize}
		\item Funding:
		\begin{itemize}
			\item[-] SystemsX.ch TPdF -- The Swiss Initiative in Systems Biology
		\end{itemize}
	\end{itemize}
\end{frame}
		
\end{document}

	